<?php

declare(strict_types=1);

namespace Skript\Utils;

use Skript\Utils\Iface\StaticFunctionProxyTrait;

require_once __DIR__.'/functions.php';

class Path
{
    use StaticFunctionProxyTrait;
    protected static $functionNamespace = __NAMESPACE__.'\\Path';
}